//Bring up the Express web library
// const mongoose =require("mongoose");
const express=require('express');
const path =require("path");
const app =express();

const viewRouter =require("./routes/viewRoutes")
const bookingRouter= require("./routes/bookingRoute") 
const userRouter =require("./routes/UserRoutes");
const feedbackRouter = require("./routes/feedbackRoute")
// const feedbackRouter = require("./routes/feedbackRoute")
//Instantiate(create) a new Express object referenced by app
// app=express()

const cookieParser = require('cookie-parser')
app.use(cookieParser())

//middleware
app.use(express.json()); //allow the express to process json
app.use(express.urlencoded({extended:true}));

app.use("/api/v1/feedback",feedbackRouter)

// app.use(express.static(__dirname+"/public"));

// booking

app.use("/api/v1/booking",bookingRouter);

app.use("/api/v1/users",userRouter);
app.use("/",viewRouter)
app.use(express.static(path.join(__dirname,"views")));




 

// app.get("/", (req,res) => {
//     res.status(200).sendFile("/dashboard.html");
// });

// const DB =process.env.DATABASE.replace(
//     "PASSWORD",
//     process.env.DATABASE_PASSWORD,

// )
// // Connecting to Local MongoDB
//  const DB = "mongodb://db:27017/agileproject" //process.env.DATABASE_LOCAL

// mongoose.connect(DB).then((con) =>{
//     console.log(con.connections)
//     console.log("DB connection successful")
// }).catch(error => console.log(error))


// // Specify the server port
// const port = 4001
// // Start listening for incoming request on the web
// app.listen(port, () => {
//     console.log(`App running on port : ${port}`)
// })

//Export this code as an object Library
module.exports =app

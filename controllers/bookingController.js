const BookingRecord = require("./../models/bookingRecord")
exports.createbooking = async(req,res,next)=>{
    const booking = new BookingRecord({...req.body,ownerData:req.user._id})
    try{
        const bookingData = await booking.save()
        res.json({data:bookingData,status:"success"})

    }catch(err){
        res.status(500).json({error:err.message})
    }
}

exports.getBookingDetailsById = async(req,res)=>{
    try{
        const bookingData = await BookingRecord.findById(req.params.id)
        const bookingDataAndUserData = await bookingData.populate('ownerData')
        res.json({data:bookingDataAndUserData,status:"success"})
        console.log(bookingData)
    }catch(err){
        res.status(500).json({error:err.message})
    }
    
}

exports.getBookingDetailsByIdForUser = async(req,res)=>{
    try{
        console.log("user jsdn,fsmn",req.user)
        const bookingData = await BookingRecord.find({ownerData:req.user.id})
        // const bookingDataAndUserData = await bookingData.populate('ownerData')
        res.json({data:bookingData,status:"success"})
        console.log(bookingData)
    }catch(err){
        res.status(500).json({error:err.message})
    }
    
}






exports.getAllBookingDetails = async(req,res,next)=>{
    try{
        const bookingData = await BookingRecord.find().populate('ownerData')
        // const bookingDataAndUserData = await bookingData.populate('ownerData')
        res.status(200).json({data:bookingData,status:"success"})

    }catch(err){
        res.status(500).json({error:err.message})
    }
}
exports.deleteBookingDataUsingId = async(req,res,next)=>{
    try {
        const user =await BookingRecord.findByIdAndDelete(req.params.id);
        res.json({ data: user,status:"success"});
    }catch(err){
        res.status(500).json({error: err.message});
    }

}

exports.updateBookingDataUsingId = async(req,res,next)=>{
    try {
        const user =await BookingRecord.findByIdAndUpdate(req.params.id,req.body);
        res.json({ data: user,status:"success"});
    }catch(err){
        res.status(500).json({error: err.message});
    }

}

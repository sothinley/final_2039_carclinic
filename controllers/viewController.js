const path =require("path")

//Login in page
exports.getLoginForm =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","signin.html"))
}

// sign up page
exports.getSignupForm =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","signup.html"))
}

// home page
exports.getHome =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","home.html"))
}


// home page
exports.getMyProfile =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","myProfile.html"))
}

// getting booking from
exports.getBookingForm = (req,res)=>{
    res.sendFile(path.join(__dirname,"../","views","booking.html"))
}




// admin home page
exports.getAdminHome =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","dashboard.html"))
}


exports.getabout =(req,res) => {
    res.sendFile(path.join(__dirname,"../","views","about.html"))
}
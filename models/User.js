const mongoose =require("mongoose")
const validator =require('validator')
const bcrypt =require("bcryptjs")
const userSchema =new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Please tell us your name!'],
    },
    email: {
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    phone: {
        type: String,
        required: [true, 'Please provide a phone number!'],
        minlength: 8,

    },
    license:{
        type: String,
        unique: true,
        required: [true, 'Please provide a license number'],
    },
    photo:{
        type:String,
        default:'default.jpg',
    },
    role:{
        type:String,
        default:"user"

    },
    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users
        select: false,
    },
    passwordConfirm:{
        type:String,
        required:[true,'Please confirm your password'], 
        validate:{
            //this only works on save!!!
            validator: function(el){
                return el === this.password
            },
            message:"Passwords are not the same",
        },
    },
    
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})

// virtule field for retriving booking detail 
userSchema.virtual('vBooking',{
    ref:'BookingRecord',
    localField:'_id',
    foreignField:'ownerData'
})

userSchema.pre("save",async function(next){
    // only run this function if password was actually modified
    if(!this.isModified("password")) return next()
    // hash this password with cost of 12
    this.password =await bcrypt.hash(this.password,12)
    // delete passwordConfirm field
    this.passwordConfirm =undefined
    next()
})
//instance method is available in all document of certain collections
userSchema.methods.correctPassword =async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User =mongoose.model('User',userSchema)
module.exports =User
const express = require('express')
const authController = require("./../controllers/authController")
const bookingController = require("./../controllers/bookingController")
const router = express.Router()


router.post("/applyBooking",
    authController.protect,
    bookingController.createbooking
)
router.get('/getBookingDataByIdForUser',
    authController.protect,
    bookingController.getBookingDetailsByIdForUser
    )

router
    .route("/:id")
    .get(authController.protect,bookingController.getBookingDetailsById)
    .delete(authController.protect,bookingController.deleteBookingDataUsingId)
    .patch(authController.protect,bookingController.updateBookingDataUsingId)

router
    .route("/")
    .get(authController.protect,bookingController.getAllBookingDetails)
module.exports = router
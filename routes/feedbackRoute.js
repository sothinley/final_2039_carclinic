const express = require('express')
const authController = require("./../controllers/authController")
const feedbackController = require("./../controllers/feedbackController")
const router = express.Router()

router.post("/",
    authController.protect,
    feedbackController.createFeedback
)
router.get("/",
    authController.protect,
    feedbackController.getAllFeedbackDetails)
router
    .route("/:id")
    .patch(authController.protect,feedbackController.updateFeedbackDataUsingId)
module.exports = router
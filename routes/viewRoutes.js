const express =require("express")
const router =express.Router()
const viewsController =require("./../controllers/viewController")
const authController = require("./../controllers/authController")

router.get("/",viewsController.getHome)
router.get("/admin",viewsController.getAdminHome)
// try{
//     router.get("/",authController.protect,authController.isAdmin,viewsController.getAdminHome)
// }catch(err){
//     router.get("/",authController.protect,viewsController.getHome)
// }
router.get("/login",viewsController.getLoginForm)
router.get("/about",viewsController.getabout)

router.get("/signup",viewsController.getSignupForm)
router.get("/myProfile",authController.protect,viewsController.getMyProfile)
router.get("/bookingForm",authController.protect,viewsController.getBookingForm)

module.exports =router
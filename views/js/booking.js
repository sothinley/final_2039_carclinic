import {showAlert} from "./alert.js"

export const bookingRegister = async (carModel,carNumber,startingDate,startingTime)=>{
    try{
        const res = await axios({
            method:"POST",
            url:'http://localhost:4001/api/v1/booking/applyBooking',
            data:{
                carModel,
                carNumber,
                startingDate,
                startingTime,
            }
            
        })

        console.log(res.data.data)
        if(res.data.status==="success"){
            showAlert('success','your request is being processed, Please wait for admin approval')
            window.setTimeout(()=>{
                location.assign('/')
            },1500)
        }


    }catch(err){
        let message = typeof err.response !== 'undefined'? 
        err.response.data.message
        :err.message
        showAlert('error',"Error : enter all the detail",message)
    }
}

document.querySelector('#bookingRegisterationButton').addEventListener('click',(e)=>{
    e.preventDefault()
    const carModel = document.getElementById('carModel').value
    const carNumber = document.getElementById('carNumber').value
    const startingDate = document.getElementById('startingDate').value
    const startingTime = document.getElementById('startingTime').value
    bookingRegister(carModel,carNumber,startingDate,startingTime)
})
fetch('http://localhost:4001/api/v1/booking/getBookingDataByIdForUser').then((jsonData)=>{
    // console.log(jsonData)//data in json formate
    return jsonData.json()//conerted to object
}).then((objectData)=>{
    console.log(objectData.data[0]._id)
    console.log(objectData.data.length)
    let tableData = ""
    let status = ""
    let button =""
    objectData.data.map((value)=>{
        if(value.userDisplay){
            if(value.status==="Accepted"){
                status=value.status
                button = ''
            }else if(value.status==="Rejected"){
                status=value.status
                button = `<button id="`+value._id+`" class="btn btn-danger" onclick="archiveUserData(${false},'${value._id}')">archive</button>`
    
            }else if(value.status==="Done"){
                status = value.status
                button = `<button id="`+value._id+`" class="btn btn-warning" onclick="archiveUserData(${false},'${value._id}')">archive</button>`
    
            }else{
                status=value.status
                button= `<button id="`+value._id+`" class="btn btn-danger" onclick="f('${value._id}')">delete</button>`
    
    
            }
            // status = value.status?"accepted":"not accepted"
            tableData+=`
            <tr>
                <td >
                    <p>${value.carModel}</p>
                </td>
                <td>
                    <p>${value.carNumber}</p>
                </td>
                <td>
                    <p>${value.startingDate}</p>
                </td>
                <td>
                    <p>${value.startingTime}</p>
                </td>
                <td>
                    <p>`+status+`</p>
                </td>
                <td>
                    `+button+`
                </td>
            </tr>`
        }
    })
    console.log(tableData)
    document.getElementById('table_body').innerHTML= tableData
})


const logout = async (req,res) =>{
    try{
        const res = await axios({
            method:'GET',
            url:'http://localhost:4001/api/v1/users/logout',
        })
        if(res.data.status === 'success'){
            window.history.pushState("", "", '/');
            location.reload(true)
        }
    }catch(err){
        showAlert('error','Error logging out ! try again')

    }
  }



import {showAlert} from './alert.js'
var obj = JSON.parse(document.cookie.substring((6)))

// var frontProfile =document.querySelector("frontProfilePicture")
// frontProfile.innerHTML=`<img src="./img/users/`+obj.photo+`" class="myprofile" alt="photo of `+obj.name+`"></img>`

var el = document.querySelector('.photorendering')
el.innerHTML=`
<div class="rounded-top text-white d-flex flex-row" style="background-color: #ed970c; height:200px;">
<div class="ms-4 mt-5 d-flex flex-column" style="width: 150px;">
  <img src="./img/users/`+obj.photo+`"
    alt="Generic placeholder image" class="img-fluid img-thumbnail mt-4 mb-2"
    style="width: 150px; z-index: 1">
</div>
<div class="ms-3" style="margin-top: 130px;">
  <h5>`+obj.name.toUpperCase()+`</h5>
</div>
</div>`


var el0 = document.querySelector(".reservationdatarendering")
if(obj.role==="user"){
  el0=`<table>
  <thead>
    <tr>
      <th scope="col">Car Model</th>
      <th scope="col">Car Number</th>
      <th scope="col">Starting Date</th>
      <th scope="col">Starting Time</th>
      <th scope="col">Status</th>
      <th scope="col">Delete</th>

    </tr>
  </thead>
  <tbody>
  
  </tbody>
</table>`
}


var el2 = document.querySelector('.userdatarendering')
el2.innerHTML =`         <h6>Information</h6>
                        <hr class="mt-0 mb-4">
                        <div class="row pt-1">
                          <div class="col-6 mb-3">
                            <h6 class="mt-1">Name</h6>
                            <br>
                            <h6 class="mt-1">Email</h6>
                            <br>
                            <h6 class="mt-1">Photo</h6>
                          </div> 
                          <div class="col-6 mb-3">
                            <input id="name"  class="mb-4" type="text" value="`+obj.name+`">
                            <input id="email" class="mb-4" type="email" value="`+obj.email+`">
                            <input id="photo" placeholder="........" class="mb-4" type="file">  
                          </div>
                        </div>
                        <hr>
                        <button type="button" class="btn btn-outline-dark btn--save-userdata" data-mdb-ripple-color="dark"style="z-index: 1;">updateData</button>`


var el3 = document.querySelector('.passwordRendering')
el3.innerHTML = `<h6 class="mt-4">Setting</h6>
<hr class="mt-0 mb-4">
<div class="row pt-1">
  <div class="col-6 mb-3">
    <h6 class="mt-1">current password</h6>
    <br>
    <h6 class="mt-1">New Password</h6>
    <br>
    <h6 class="mt-1">Confirm Password</h6>
  </div>
  <div class="col-6 mb-3">
      <input id="password-current" placeholder="........" class="mb-4" type="password">
      <input id="password" placeholder="........" class="mb-4" type="password">
      <input id="password-confirm" placeholder="........" class="mb-4" type="password">
  </div>
</div>
<hr>
<div class="">
  <button type="button" class="btn btn-outline-dark btn--save-password" data-mdb-ripple-color="dark" style="z-index: 1;">save password</button>
</div>`

//Updating settings
 
// type is either 'password' or data
export const updateSettings = async (data, type) => {
    try {
      const url =
        type === 'password'
          ? 'http://localhost:4001/api/v1/users/updateMyPassword'
          : 'http://localhost:4001/api/v1/users/updateMe'
      const res = await axios({
        method: 'PATCH',
        url,
        data,
      })
      console.log(res.data.status)
      if (res.data.status === 'success') {
        showAlert('success', 'Data updated successfully!')
      }
    } catch (err) {
      let message =
        typeof err.response !== 'undefined'
          ? err.response.data.message
          : err.message
      // showAlert('error', 'Error: Please provide valid email address', message)
      showAlert('error', err.response.data.message)
    }
  }
 
  const userDataForm = document.querySelector('.btn--save-userdata')
  console.log("userDataForm",userDataForm)
  userDataForm.addEventListener('click', (e) => {
    e.preventDefault()
    console.log("kjashdkjashdlkajsh")
    var obj = JSON.parse(document.cookie.substring(6))
    const form = new FormData()
    form.append('name', document.getElementById('name').value)
    form.append('email', document.getElementById('email').value)
    form.append('photo', document.getElementById('photo').files[0])
    form.append('userId', obj._id)
    console.log(form)
    updateSettings(form, 'data')
  })
 
  const userPasswordForm = document.querySelector('.btn--save-password')
  userPasswordForm.addEventListener('click', async (e) => {
    e.preventDefault()
    document.querySelector('.btn--save-password').textContent = 'Updating...'
    const passwordCurrent = document.getElementById('password-current').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('password-confirm').value
    await updateSettings(
      { passwordCurrent, password, passwordConfirm },
      'password',
    )
    document.querySelector('.btn--save-password').textContent = 'Save password'
    document.getElementById('password-current').value = ''
    document.getElementById('password').value = ''
    document.getElementById('password-confirm').value = ''
  })


  



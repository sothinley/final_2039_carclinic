// import {showAlert} from "./alert.js"

// export const signup = async(name,emaillicensee,password,passwordConfirm,llicense=>{
//     try{
//         console.log("name",name)
//         const res = await axios({
//             method:'POST',
//             url:'http://localhost:4001/api/v1/users/signup',
//             data:{
//                 name,
//                 email,
//                 phone,
//                 license,
//                 password,
//                 passwordConfirm,
//             },
            
//         })
//         console.log("statue",res.data.status)
//         if(res.data.status==='success'){
//             showAlert('success','Account created successfully')
//             window.setTimeout(()=>{
//                 location.assign('/')
//             },1500)
//         }

//     }catch(err){
//         let message = typeof err.response !== 'undefined'? 
//         err.response.data.message
//         :err.message
//         showAlert('error',err.message,message)
//     }
// }
// document.querySelector('.form').addEventListener('submit',(e)=>{
//     e.preventDefault()
//     const name = document.getElementById('name').value
//     console.log("name check",name)
//     const email = document.getElementById('email').value
//     const password = document.getElementById('password').value
//     console.log("password check",password)
//     const passwordConfirm = document.getElementById('confirmpassword').value
//     console.log("confirm check",passwordConfirm)
//     const phone = document.getElementById('contact').value
//     // const role = document.querySelector('input[name=role]:checked').value
//     const license = document.getElementById('license').value
//     signup(name,email,phone,password,passwordConfirm,license)

// })



import {showAlert} from "./alert.js"
// import axios from 'axios'
// const axios = require('axios')

export const signup = async(name,email,phone,license,password,passwordConfirm)=>{
    try{
        const res = await axios({
            method:'POST',
            url:'http://localhost:4001/api/v1/users/signup',
            data:{
                name,
                email,
                phone,
                license,
                password,
                passwordConfirm,
            },
        })
        console.log(res.data.data)
        if(res.data.status==='success'){
            showAlert('success','Account created successfully')
            window.setTimeout(()=>{
                location.assign('/')
            },1500)
        }

    }catch(err){
        let message = typeof err.response !== 'undefined'? 
        err.response.data.message
        :err.message
        showAlert('error',"Error : password are not same",message)
    }
}
document.querySelector('.form').addEventListener('submit',(e)=>{
    // e.preventDefault()
    e.preventDefault()
    const name = document.getElementById('name').value
    console.log('name',name)
    const email = document.getElementById('email').value
    const phone = document.getElementById('contact').value
    const license = document.getElementById('license').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('confirmpassword').value
    // const role = document.querySelector('input[name=role]:checked').value
    signup(name,email,phone,license,password,passwordConfirm)

})
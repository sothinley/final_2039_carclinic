import {showAlert} from "./alert.js"

export const feedbackSubmit = async (feedback)=>{
    try{
        const res = await axios({
            method:"POST",
            url:'http://localhost:4001/api/v1/feedback',
            data:{
                feedback,
            }
            
        })

        console.log(res.data.data)
        if(res.data.status==="success"){
            showAlert('success','thank you for your feedback')
            window.setTimeout(()=>{
                location.reload(true)
            },1500)
        }


    }catch(err){
        let message = typeof err.response !== 'You are not Loged In'? 
        err.response.data.message
        :err.message
        showAlert('error',"You Are not looged in",message)
    }
}

document.querySelector('#feedbackSubmitButton').addEventListener('click',(e)=>{
    e.preventDefault()
    const feedback = document.getElementById('feedback').value
    feedbackSubmit(feedback)
})



import { showAlert } from './alert.js'

export const bookingRegister = async (data) => {
    try {
        const res = await axios({
            method: 'POST',
            url: "http://localhost:4001/api/v1/bookings",
            data
        });

        if(res.data.status==="success"){
            showAlert('success', 'Success', 'Data Update successfully');
            window.setTimeout(()=>{
                location.reload()
            },1500)
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message;
        showAlert('error', message);
    }
};

const userDataForm = document.querySelector('.booking-car');

userDataForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const carModel = document.getElementById('carModel').value;
    const carColor = document.getElementById('vehicleColour').value;
    const carNumber = document.getElementById('carPlateNumber').value;
    const phoneNumber = document.getElementById('phoneNumber').value;
    const startingTime = document.getElementById('startingTime').value;
    const startingDate = document.getElementById('startingDate').value;
    
    const data = {
        carModel,
        carColor,
        carNumber,
        phoneNumber,
        startingTime,
        startingDate
    };
    
    bookingRegister(data);
});


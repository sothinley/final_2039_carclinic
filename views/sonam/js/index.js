// const { json } = require("express")
import {showAlert} from "./alert.js"

var obj
if(document.cookie){
    // obj = JSON.parse(document.cookie.substring(6))
    obj = JSON.parse(document.cookie.substring(6))
    // console.log(';askndkansd')
}else{
    obj = JSON.parse('{}')
}
var el = document.querySelector('.sf-menu')
if(obj._id){

    el.innerHTML=el.innerHTML+
   
    '<li><a id="logout" href="#" style="font-weight: bold; color: black;">Logout</a></li>'

    var doc = document.querySelector('#logout')
    doc.addEventListener('click',(e)=>logout())
}else{
    el.innerHTML = ''
    
}
const logout = async () =>{
    try{
        const res = await axios({
            method:'GET',
            url:'http://localhost:4001/api/v1/users/logout',
        })
        if(res.data.status === 'success'){
            window.setTimeout(() => {
                window.location.href = '/landingindex.html'; // modify this line to redirect to index.html
              }, 1500)
        }
    }catch(err){
        showAlert('error','Error logging out ! try again')

    }
}
